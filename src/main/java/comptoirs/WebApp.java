package comptoirs;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.lang.NonNull;
import org.springframework.web.servlet.config.annotation.*;

@SpringBootApplication
public class WebApp {

	public static void main(String[] args) {
		SpringApplication.run(WebApp.class, args);
	}

	@Bean
	ModelMapper modelMapper() {
		return new ModelMapper();
	}

//	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(@NonNull CorsRegistry registry) {
				registry.addMapping("/services/commandes/expedier/*")
						.allowedOrigins("http://localhost:5173");
				registry.addMapping("/services/commandes/ajouterLigne")
						.allowedOrigins("http://localhost:5173");
			}
		};
	}
}
