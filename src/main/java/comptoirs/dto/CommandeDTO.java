package comptoirs.dto;

import java.time.LocalDate;
import java.util.List;

import lombok.Data;

@Data
public class CommandeDTO {
    private Integer numero;
    private LocalDate saisiele;
    private ClientDTO client;
    private List<LigneDTO> lignes;
}
